import './App.css';
import UserForm from './components/Form';

function App() {
  return (
    <div className="container">
      <UserForm/>
    </div>
  );
}

export default App;

